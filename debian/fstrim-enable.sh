#!/bin/bash

# This if for Debian 10, but should work for any newer debian based OS.
#
# This is highly useful in vm environments with discard support. 

if [[ $EUID -ne 0 ]]; then
   echo "This script must be run as root" 
   exit 1
fi

systemctl enable fstrim.timer
systemctl start fstrim.timer